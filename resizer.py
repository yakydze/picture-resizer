import os
import sys
from PIL import Image


def resize(filepath, width, height):
    size = height, width
    infile = filepath
    outfile = "{}.thumbnail{}".format(
        os.path.splitext(infile)[0],
        os.path.splitext(infile)[-1]
    )
    im = Image.open(infile)
    im = im.resize(size)
    if os.path.splitext(infile)[-1].lower() == 'jpg':
        im.save(outfile, "JPG")
    else:
        im.save(outfile, "PNG")


if __name__ == '__main__':
    print("This is the name of the script: ", sys.argv[0])
    print("Number of arguments: ", len(sys.argv))
    print("The arguments are: ", str(sys.argv))
    resize(sys.argv[1], int(sys.argv[2]), int(sys.argv[3]))
