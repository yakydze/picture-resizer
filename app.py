from flask import Flask, jsonify, flash, redirect, request, send_file
import os
import subprocess
from datetime import datetime
from werkzeug.utils import secure_filename
app = Flask(__name__)

orderbook = dict()
ordernum = 0

ALLOWED_EXTENSIONS = set(['png', 'jpg'])

for filepath in os.listdir('obj'):
    os.remove('obj/{}'.format(filepath))


def allowed_file(filename):
    return (
        '.' in filename and
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
    )


def extract_hw(data):
    if 'width' not in data or 'height' not in data:
        raise IOError('Height and width are required')

    width, height = data['width'], data['height']
    if not (width and height):
        raise IOError('Height and width should be stated')

    width, height = int(width), int(height)
    if width <= 0 or height <= 0:
        raise IOError('Height and width should be positive')
    if width >= 10000 or height >= 10000:
        raise IOError('Height and width should be less than 10000')

    return width, height


@app.errorhandler(405)
def method_not_allowed(error):
    return jsonify(
        {'Error': 'This method is not allowed'}
    ), 405


@app.errorhandler(404)
def page_not_found(error):
    return jsonify(
        {'Error': 'This page does not exist'}
    ), 404


def new_order():
    global ordernum
    ordernum += 1
    orderbook[str(ordernum)] = {
        'status': 'Processing',
        'started': datetime.now(),
        'filepath': None,
        'height': 0,
        'width': 0
    }
    return str(ordernum)


@app.route('/resize', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return redirect(request.url)
    file = request.files['file']
    form_data = {key: value for key, value in request.form.items()}
    try:
        width, height = extract_hw(form_data)
    except OSError as e:
        return jsonify({'Input Error': str(e)}), 400
    except ValueError as e:
        return (
            jsonify(
                {
                    'Input Error': 'Height and width should be integer numbers'
                }
            ),
            400
        )
    if file.filename == '':
        return jsonify({'Input Error': 'Not allowed file type'}), 400
    if file and allowed_file(file.filename):
        order = new_order()
        filepath = 'obj/{}.{}'.format(
            str(order),
            file.filename.rsplit('.', 1)[1].lower()
        )
        targetpath = 'obj/{}.thumbnail.{}'.format(
            str(order),
            file.filename.rsplit('.', 1)[1].lower()
        )
        file.save(filepath)
        orderbook[order]['filepath'] = filepath
        orderbook[order]['targetpath'] = targetpath
        orderbook[order]['height'] = height
        orderbook[order]['width'] = width
        program = "py resizer.py {} {} {}".format(filepath, width, height)
        subprocess.Popen(program)

        return jsonify(
            {
                'Order Number': order,
                'Order Status': orderbook[order]['status'],
                'Order Started': orderbook[order]['started'],
                'New width': '{}px'.format(orderbook[order]['width']),
                'New height': '{}px'.format(orderbook[order]['height']),
                'Status URL': request.url.replace(
                    '/resize',
                    '/status/{}'.format(order)
                )
            }
        ), 201
    else:
        return jsonify({'Error': 'Not allowed file type'}), 400


@app.route("/status/<order_id>", methods=['GET'])
def get_proceeding(order_id):
    if order_id in orderbook:
        filepath = orderbook[order_id]['targetpath']
        if os.path.isfile(filepath):
            return jsonify(
                {
                    'Order Number': order_id,
                    'Order Status': 'Ready',
                    'Order Started': orderbook[order_id]['started'],
                    'Result URL': request.url.replace('/status/', '/fetch/')
                }
            ), 200
        else:
            return jsonify(
                {
                    'Order Number': order_id,
                    'Order Status': orderbook[order_id]['status'],
                    'Order Started': orderbook[order_id]['started'],
                    'New width': '{}px'.format(orderbook[order_id]['width']),
                    'New height': '{}px'.format(orderbook[order_id]['height']),
                }
            ), 200
    else:
        return jsonify(
            {
                'Order Number': order_id,
                'Error': 'Order not found',
            }
        ), 404


@app.route("/fetch/<order_id>", methods=['GET'])
def get_result(order_id):
    if order_id in orderbook:
        filepath = orderbook[order_id]['targetpath']
        if os.path.isfile(filepath):
            return send_file(filepath), 200
        else:
            return jsonify(
                {
                    'Order Number': order_id,
                    'Error': 'Result not found',
                }
            ), 404
    else:
        return jsonify(
            {
                'Order Number': order_id,
                'Error': 'Order not found',
            }
        ), 404
