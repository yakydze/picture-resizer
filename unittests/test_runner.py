import unittest
import requests
import time
import os
from PIL import Image


class TestStringMethods(unittest.TestCase):

    def test_not_found(self):
        self.assertEqual(
            requests.get('http://127.0.0.1:5000/status/0').status_code,
            404
        )
        self.assertEqual(
            requests.get('http://127.0.0.1:5000/fetch/0').status_code,
            404
        )
        self.assertEqual(
            requests.get('http://127.0.0.1:5000/').status_code,
            404
        )

    def test_not_allowed(self):
        self.assertEqual(
            requests.get('http://127.0.0.1:5000/resize').status_code,
            405
        )
        self.assertEqual(
            requests.post('http://127.0.0.1:5000/fetch/1').status_code,
            405
        )
        self.assertEqual(
            requests.post('http://127.0.0.1:5000/status/1').status_code,
            405
        )

    @staticmethod
    def post_resize(headers, data, files):
        return requests.post(
                'http://127.0.0.1:5000/resize',
                headers=headers, data=data, files=files
            ).status_code

    def test_bad_input(self):
        headers = {'cache-control': "no-cache"}
        with open('test.png', 'rb') as source:
            self.assertEqual(
                400,
                self.post_resize(
                    headers,
                    {'height': '150'},
                    {'file': source}
                )
            )
            self.assertEqual(
                400,
                self.post_resize(
                    headers,
                    {'width': '150'},
                    {'file': source}
                )
            )
            self.assertEqual(
                400,
                self.post_resize(
                    headers,
                    {'height': '1.2', 'width': '150'},
                    {'file': source}
                )
            )
            self.assertEqual(
                400,
                self.post_resize(
                    headers,
                    {'height': '150', 'width': '1.2'},
                    {'file': source}
                )
            )
            self.assertEqual(
                400,
                self.post_resize(
                    headers,
                    {'height': '-1', 'width': '150'},
                    {'file': source}
                )
            )
            self.assertEqual(
                400,
                self.post_resize(
                    headers,
                    {'height': '150', 'width': '-1'},
                    {'file': source}
                )
            )
            self.assertEqual(
                400,
                self.post_resize(
                    headers,
                    {'height': '150', 'width': '150'},
                    {}
                )
            )
            self.assertEqual(
                400,
                self.post_resize(
                    headers,
                    {'height': '0', 'width': '150'},
                    {'file': source}
                )
            )
            self.assertEqual(
                400,
                self.post_resize(
                    headers,
                    {'height': '150', 'width': '0'},
                    {'file': source}
                )
            )
            self.assertEqual(
                400,
                self.post_resize(
                    headers,
                    {'height': '10000', 'width': '150'},
                    {'file': source}
                )
            )
            self.assertEqual(
                400,
                self.post_resize(
                    headers,
                    {'height': '150', 'width': '10000'},
                    {'file': source}
                )
            )
        with open('test_runner.py', 'rb') as source:
            self.assertEqual(
                400,
                self.post_resize(
                    headers,
                    {'height': '150', 'width': '150'},
                    {'file': source}
                )
            )

    def test_result(self):

        with open('test.png', 'rb') as source:

            headers = {
                'cache-control': "no-cache",
            }
            data = {
                'height': '150',
                'width': '150',
            }
            files = {
                'file': source
            }

            response = requests.post(
                'http://127.0.0.1:5000/resize',
                headers=headers, data=data, files=files
            )

        self.assertEqual(
            response.status_code,
            201
        )

        data = response.json()

        self.assertIn('Status URL', data)

        response = requests.get(
            data['Status URL']
        )

        self.assertEqual(
            response.status_code,
            200
        )

        while 'Result URL' not in response.json():
            time.sleep(1)
            response = requests.get(
                data['Status URL']
            )

        data = response.json()
        response = requests.get(
            data['Result URL']
        )

        self.assertEqual(
            response.status_code,
            200
        )
        with open('result.thumbnail.png', 'wb') as result:
            result.write(response.content)

        result = Image.open('result.thumbnail.png')
        test = Image.open('test.thumbnail.png')

        self.assertEqual(result, test)

        os.remove('result.thumbnail.png')


if __name__ == '__main__':
    unittest.main()
